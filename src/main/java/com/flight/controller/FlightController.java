package com.flight.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.flight.dto.FlightDto;
import com.flight.model.Flight;
import com.flight.service.FlightService;

@RestController
public class FlightController {

	@Autowired
	FlightService flightService;

	@PostMapping(value = "/flights")
	public ResponseEntity<List<Flight>> searchFlight(@RequestBody FlightDto flightDto) {
		List<Flight> flights = flightService.searchFlight(flightDto);
		return new ResponseEntity<>(flights, HttpStatus.OK);

	}

}
