package com.flight.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.flight.dto.BookingDto;
import com.flight.model.Booking;
import com.flight.service.FlightBookingService;

@RestController
public class FlightBookingController {

	@Autowired
	FlightBookingService flightBookingService;
	
	@PostMapping("booking")
	public ResponseEntity<Booking> makeBooking(@RequestBody BookingDto bookingDto) {
	
	Booking booking=flightBookingService.makeBooking(bookingDto);
		 
		 return new ResponseEntity<>(booking,HttpStatus.OK);
		
	}
	
	@GetMapping(value = "/booking/{id}")
	public ResponseEntity<Booking> getBookingById(@PathVariable("id") int id) {
		Booking booking = flightBookingService.getBookingById(id);
		return new ResponseEntity<>(booking, HttpStatus.OK);

	}

}
