package com.flight.service;

import com.flight.dto.BookingDto;
import com.flight.model.Booking;


public interface FlightBookingService {

	Booking makeBooking(BookingDto bookingDto);

	Booking getBookingById(int id);

	

}
