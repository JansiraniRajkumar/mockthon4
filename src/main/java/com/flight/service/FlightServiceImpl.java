package com.flight.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.flight.dto.FlightDto;
import com.flight.exception.FlightNotFoundException;
import com.flight.model.Flight;
import com.flight.repository.FlightRepo;


@Service
public class FlightServiceImpl implements FlightService {
	@Autowired
	FlightRepo flightRepository;

	@Override
	public List<Flight> searchFlight(FlightDto flightDto) throws FlightNotFoundException {
		List<Flight> flights = flightRepository.findFlightBySourceAndDestinationAndJourneyDate(flightDto.getSource(),
				flightDto.getDestination(), flightDto.getJourneyDate());
		if (flights.isEmpty()) {
			throw new FlightNotFoundException("flight is not available");
		}
		return flights;
	}

}
