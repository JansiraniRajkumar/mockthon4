package com.flight.controller;

import java.util.ArrayList;
import java.util.List;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.flight.dto.FlightDto;
import com.flight.model.Flight;
import com.flight.service.FlightService;


@RunWith(MockitoJUnitRunner.Silent.class)
public class FlightControllerTest {

	@InjectMocks
	FlightController flightController;

	@Mock
	FlightService flightService;

	static Flight flight = null;

	@BeforeClass
	public static void setUp() {
		flight = new Flight();

	}

	@Test
	public void testsearchFlightForPositive() {
		List<Flight> flights = new ArrayList<Flight>();
		Flight flight = new Flight();
		FlightDto flightDto = new FlightDto();
		flight.setFlightId(1);
		flight.setSource("mumbai");
		flight.setDestination("bangalore");
		Mockito.when(flightService.searchFlight(flightDto)).thenReturn(flights);
		ResponseEntity<List<Flight>> flights1 = flightController.searchFlight(flightDto);
		Assert.assertNotNull(flights1);
		Assert.assertEquals(flights1.getStatusCode(), HttpStatus.OK);

	}

	@Test
	public void testsearchFlightForNagative() {
		List<Flight> flights = new ArrayList<Flight>();
		Flight flight = new Flight();
		flight.setFlightId(-1);
		FlightDto flightDto = new FlightDto();
		flight.setSource("mumbai");
		flight.setDestination("Bangalore");
		Mockito.when(flightService.searchFlight(flightDto)).thenReturn(flights);
		ResponseEntity<List<Flight>> flights1 = flightController.searchFlight(flightDto);
		Assert.assertNotNull(flights1);
		Assert.assertEquals(flights1.getStatusCode(), HttpStatus.OK);
	}

	@AfterClass
	public static void tearDown() {
		flight = null;
	}

}
